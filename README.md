![DF](dficon.png "DFLinux")

# DFlinux

A linux based OS with Dwarf Fortress

## Contemplating the base of DFlinux

The project is in a bit of a stall. I could base DFlinux upon Arch Linux and create an ISO with archiso. This is not that difficult to do. But then again, it is Arch. Then there is
always the other choice: Debian GNU/Linux. Debian is stable and works. I could base it on Buster even. Which would mean that the install is done using Calamares and thus installing
from the live session is a possibility. And then again, Debian is stable, very stable.

So, that's where I am. Shall I use Arch of Debian? Okay, Debian it is...

## eznixOS
Whilst browsing Youtube I came across the adventures of Eznix and he showed how easy it is to use Live Build and create an Debian based OS. He even created some scripts to do it
automatically. As I do not whish to redistribute his scripts I will use them as an inspiration for my own. The first release (of the scripts) will be able to build Debian Buster
based OS  and it will feature a fulle working XFCE4 desktop environment with th Calamares installer.

No Dwarf Fortress yet nor any of the dependencies for DF. That will be the first release of the ISO.

## Dwarf Fortress
I will not be distributing the Lazy Newb pack. Those can be downloaded from de DFFD. It will contain the latest version of Dwarf Fortress, DFhack, TWBT and some tilingsets. Whether
Soundsense, Soundsence or other tools will be included I have not decided yet.